﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsMatrixMultiplication
{
    public partial class _Default : Page
    {
        Matrix matrix1;
        Matrix matrix2;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            matrix1 = new Matrix(FileUpload1);
            matrix2 = new Matrix(FileUpload2);
            MatrixMultiplier multiplier = new MatrixMultiplier();
            Matrix result = multiplier.Multiply(matrix1, matrix2);
            String resultMatrix = "";
            foreach(var row in result.list)
            {
                foreach(var col in row)
                {
                    resultMatrix += col + " ";
                }
                resultMatrix += "<br>";
            }
            Literal1.Text = resultMatrix;
        }
    }


    public partial class Matrix : Page
    {
        public List<List<double>> list;

        public Matrix()
        {
            list = new List<List<double>>();
        }

        public Matrix(FileUpload inputFile)
        {
            StreamReader streamReader = new StreamReader(inputFile.PostedFile.InputStream);
            String content = streamReader.ReadToEnd();

            list = new List<List<double>>();

            int i = 0, j = 0;
            foreach (var row in content.Split('\n'))
            {
                j = 0;
                List<double> listToAdd = new List<double>();
                foreach (var col in row.Trim().Split(' '))
                {
                    listToAdd.Add(double.Parse(col.Trim()));
                    j++;
                }
                list.Add(listToAdd);
                i++;
            }
        }

        public int GetRows()
        {
            return list.Count;
        }

        public int GetCols()
        {
            return list[0].Count;
        }
    }

    public partial class MatrixMultiplier : Page
    {
        public Matrix Multiply(Matrix matrix1, Matrix matrix2)
        {
            Matrix result = new Matrix();

            for(int i=0; i < matrix1.list.Count; i++)
            {
                result.list.Add(new List<double>());
            }

            Parallel.For(0, matrix1.GetRows(), i =>
                 {
                     List<double> matrixRow = new List<double>();
                     for (int j = 0; j < matrix2.GetCols(); ++j)
                     {
                         double resultValue = 0;
                         
                         for (int k = 0; k < matrix1.GetCols(); ++k)
                         {
                             resultValue += matrix1.list[i][k] * matrix2.list[k][j];
                         }
                         matrixRow.Add(resultValue);                        
                     }
                     result.list[i] = matrixRow;
                 }
            ) ;
       
            var cols = result.GetCols();
            var rows = result.GetRows();
            return result;
        }
    }
}