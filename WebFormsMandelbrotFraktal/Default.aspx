﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebFormsMandelbrotFraktal._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-12">
            <asp:Image ID="Image1" runat="server" />
        </div>
        <div class="col-md-12">
            <asp:Label ID="Label1" runat="server" Text="Height"></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-12">
            <asp:Label ID="Label2" runat="server" Text="Width"></asp:Label>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-12">
            <asp:Label ID="Label3" runat="server" Text="Minimum"></asp:Label>
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-12">
            <asp:Label ID="Label4" runat="server" Text="Maksimum"></asp:Label>
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-12">
            <asp:Label ID="Label5" runat="server" Text="Minimum Im"></asp:Label>
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-12">
            <asp:Label ID="Label6" runat="server" Text="Iterations"></asp:Label>
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-12">
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        </div>
    </div>

</asp:Content>
