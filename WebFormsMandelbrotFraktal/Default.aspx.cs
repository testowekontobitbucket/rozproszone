﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFormsMandelbrotFraktal
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
      
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int ImageHeight = int.Parse(TextBox1.Text);
            int ImageWidth = int.Parse(TextBox2.Text);
            Bitmap img = new Bitmap(ImageHeight, ImageWidth);
            byte[][] byteArray = new byte[ImageHeight][];
            double MinRe = double.Parse(TextBox3.Text);
            double MaxRe = double.Parse(TextBox4.Text);
            double MinIm = double.Parse(TextBox5.Text);
            int MaxIterations = int.Parse(TextBox6.Text);

            double MaxIm = MinIm + (MaxRe - MinRe) * ImageHeight / ImageWidth;
            double Re_factor = (MaxRe - MinRe) / (ImageWidth - 1);
            double Im_factor = (MaxIm - MinIm) / (ImageHeight - 1);

            Parallel.For(0, ImageHeight, y =>
            {
                byteArray[y] = new byte[ImageWidth];
                double c_im = MaxIm - y * Im_factor;
                Parallel.For(0, ImageWidth, x =>
                {
                    double c_re = MinRe + x * Re_factor;

                    double Z_re = c_re, Z_im = c_im;
                    bool isInside = true;
                    for (int n = 0; n < MaxIterations; ++n)
                    {
                        double Z_re2 = Z_re * Z_re, Z_im2 = Z_im * Z_im;
                        if (Z_re2 + Z_im2 > 4)
                        {
                            isInside = false;
                            break;
                        }
                        Z_im = 2 * Z_re * Z_im + c_im;
                        Z_re = Z_re2 - Z_im2 + c_re;
                    }
                    if (isInside)
                    {
                        byteArray[y][x] = 255;
                    }
                });
            });

            for (int i = 0; i < ImageWidth; i++)
            {
                for (int j = 0; j < ImageHeight; j++)
                {
                    if (byteArray[i][j] == 255)
                    {
                        img.SetPixel(j, i, Color.White);
                    }
                }
            }

            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Gif);
            var base64Data = Convert.ToBase64String(ms.ToArray());
            Image1.Attributes.Add("src", "data:image/gif;base64," + base64Data);

        }
    }
}